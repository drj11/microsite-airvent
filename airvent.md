[brutal]: #title "Airvent"
[brutal]: #dateNoDisplay "2023-05-17"
[brutal]: #authorNoDisplay "drj"

<style>
body {
  font-family: 'Airvent', serif;
  margin: 0.75rem;
}

h1 {
  font-family: 'Airvent', serif;
  font-size: 3rem;
  font-weight: normal;
  margin-bottom: 2px;
  margin-block-start: 0;
}

h2 {
  font-size: 2rem;
  font-weight: normal;
  margin-block-end: 0.75rem;
  margin-block-start: 0.75rem;
  margin-left: -0.75rem;
  margin-right: -0.75rem;
  padding: 0.25rem 0.75rem 0.125rem;
  background-color: #222;
  color: #fff
}

button {
  background-color: #777;
  font-family: 'Ubuntu Mono';
}

div > p:first-child {
  margin-top: 0;
}

div > p:last-child {
  margin-bottom: 0;
}

div > p {
  margin-bottom: 1rem;
}

.display {
  font-size: 200%;
  padding: 3rem;
}

.caption.caption {
  font-size: 16px;
  margin: 16px;
  position: absolute;
}

.centre {
  zdisplay: flex;
  zalign-items: center;
  zjustify-content: center;
}

.DarkMid {
  background-color: #000;
  color: #777;
}

.DarkLightest {
  background-color: #000;
  color: #fff;
}

.LightestDark {
  background-color: #fff;
  color: #000;
}

.LightestMid {
  background-color: #fff;
  color: #777;
}

.MidDark {
  background-color: #777;
  color: #000;
}

.MidDark .secondary {
  color: #ffff;
}

.MidLightest {
  background-color: #777;
  color: #fff;
}

.secondary {
  font-size: 16px;
}

.frac {
  font-feature-settings: "frac";
}

.onum {
  font-feature-settings: "onum";
}

.smcp {
  font-feature-settings: "smcp";
}

tt, code, kbd, samp {
    font-family: 'Ubuntu Mono';
}

pre, xmp, plaintext, listing {
    font-family: 'Ubuntu Mono';
}

/* reduce the indent */
ul {
  padding-left: 1em;
}

figure {
    margin-left: 1.5em;
    margin-right: 1.5em;
}

@font-face {
  font-family: 'Airvent';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(static/OTF/AirventLAB20230731-Regular.otf) format('opentype');
}

@font-face {
  font-family: 'Ubuntu Mono';
  font-style: normal;
  font-weight: 500;
  font-display: swap;
  src: url(static/TTF/UbuntuMono-Regular.ttf) format('opentype');
}

</style>

A utility font from <a href="https://about.cubictype.com/">CubicType</a>.

Made in <span class="smcp">Yorkshire</span> by David Jones.

<div class="MidDark display centre">
<p>
AÆBCDÐEƏƷFGHIJ
<br/>
KLMNƝŊOŒPÞQ
<br/>
RSꞋẞTUVWXYZ
</p>
<p class="secondary">Capitals</p>
<p>
aæbcdðeəʒfghij
<br/>
kĸlmnɲŋoœpþq
<br/>
rsꞌßtuvwxyz
</p>
<p class="secondary">Little letters</p>
<p>
*0123456789!?
<br/>
99$@100%
</p>
<p class="secondary">Numbers &c</p>
<p class="smcp">
aæbcdðeəʒfghij
<br/>
kĸlmnɲŋoœpþq
<br/>
rsꞌßtuvwxyz
<br/>
&0123456789$
</p>
<p class="secondary">Smallcaps</p>
</div>

This font informs.

Airvent works best in short staccato bursts,
but is also calm enough to be used in longer texts.

<div class="LightestMid">
<p class="caption">Edit me!</p>
<div class="LightestDark display" style="border: 8px double">
<p style="font-size: 48px" contenteditable>
Big
</p>
<p style="font-size: 16px" contenteditable>
Small
</p>
<p style="font-size: 24px" contenteditable>
In the middle
</p>
</div>
</div>

## Design notes

Airvent is a utility sans with near-monoline strokes with
rounded terminals.

Airvent comes with a complete set of built-in vulgar fractions
and more can be composed via the `frac` feature.

<div class="frac MidLightest">
<p class="caption">Edit me!</p>
<p class="MidDark display" contenteditable>
The halves 1/2, 3/6, 4/8, and the half knots 5/7, 9/10
</p>
</div>

Airvent has smallcaps, including smallcaps numbers,
subscript and superscript numbers, and punctuation for capitals.

Airvent has reasonable Latin coverage with over 500 glyphs and
support for more than 60 languages,
including localisations for
Catalan, Dutch.


## Origins and interpretation

The core capital alphabet and numbers for Airvent began as a
trace from a bad scan of a military standard for aircraft
instrument lettering.
While most of the glyph shapes shown in that standard
are fairly mainstream, some are quirky from a typographical
perspective, and others are drawn with inconsistent weight.
I suspect that the showing in the standard is meant more
as a guide than a strict prescription.

Some of these quirks were so unsettling that i have replaced
them with more harmonised forms and put the drawings that
are closer to the scan in alts.
A few forms i have also relegated to alts, not
because their harmony was unsettling, but
because they were too similar in shape to other letters,
compromising legibility.
The new legible forms are:

- 0 drawn with dot
- 1 drawn with flag
- 7 drawn with crossbar
- I draw with short horizontals

The original standard only covers a very basic repertoire of
capital **A** to **Z** and numbers **0** through **9**.
Many new glyphs have been drawn.
These are generally drawn slightly more conventionally and
harmoniously than the original core, and
occasionally with little rule-breaking tweaks.


## Capital paradox

Airvent began as a capital-only font and
it still works best in this role.
For example, numbers and punctuation are more closely harmonised
to the capital letters;
Spacing of capital letters prioritises capital-only settings.

The OpenType Layout feature `case` is used to provide harmonised
punctuation for capital letters.
This feature assumes that the default punctuation is harmonised
for lowercase or mixed setting and that `case` should be enabled
for all-caps setting.
Ironically, that means that if you are setting in all-caps,
you have to make sure that you enable the `case` feature to get
the correct punctuation.
The punctuation affected is:

- dashes and hyphens
- guillemets
- inverted question and exclamation marks


## Examples

Planets
<p class="DarkLightest display">
☉ Sun •
☿ Mercury •
♀ Venus •
♁ Earth •
♂ Mars •
♃ Jupiter •
♄ Saturn •
♆ Neptune •
⛢ Uranus •
</p>

# Tara rŵan!
